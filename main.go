package main

import (
	"gitlab.com/Christopher.Lim/rest-api/db"
	"gitlab.com/Christopher.Lim/rest-api/routes"
	"github.com/gin-gonic/gin"
)

func main() {
	db.InitDB()
	server := gin.Default() //config the http server

	routes.RegisterRoutes(server)

	server.Run(":8080") // localhost:8080
}

