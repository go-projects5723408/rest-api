# Rest-API
This is a sample project for Go in using Rest Api

# Installation
- To install the dependencies, please install GO by going to this url https://go.dev/doc/install and follow the instructions

# Extension
- Go (Go Team at Google go.dev) 
- REST Client (REST Client for Visual Studio Code)